VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   6450
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3585
   BeginProperty Font 
      Name            =   "Courier"
      Size            =   9.75
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   6450
   ScaleWidth      =   3585
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "Clear"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1920
      TabIndex        =   2
      Top             =   120
      Width           =   1455
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "Courier"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5055
      Left            =   240
      MultiLine       =   -1  'True
      TabIndex        =   1
      Text            =   "Form1.frx":0000
      Top             =   1080
      Width           =   3135
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Test"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1455
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

Dim Dict1 As Dictionary
Set Dict1 = New Dictionary

Dim Dict2 As Dictionary
Set Dict2 = New Dictionary

Dim Dict0 As Dictionary
Set Dict0 = New Dictionary

Dim key As Variant
Dim Matriz1(1, 100) As String

Dim maxD As Integer


Text1.Text = Empty


With Dict1
    For i = 1 To 9
        .Add "clave1-" & i, "valor1-" & i
    Next
End With

With Dict2
    For i = 1 To 9
        .Add "clave2-" & i, "valor2-" & i
    Next
End With


With Dict0
    .Add 1, Dict1
    .Add 2, Dict2
End With


maxD = 0

For g = 1 To Dict0.Count
    If Dict0.Item(g).Count > maxD Then
        maxD = Dict0.Item(g).Count
    End If
Next

Dim x As Integer
Dim y As Integer

x = 0
y = 0

For o = 1 To Dict0.Count

    For Each key In Dict0.Item(o).Keys
        Text1.Text = Text1.Text & key & ": " & Dict0.Item(o).Item(key) & vbCrLf
        Matriz1(x, y) = key
        Matriz1(x, y + 1) = Dict0.Item(o).Item(key)
        y = y + 2
    Next key
    Text1.Text = Text1.Text & vbCrLf
    x = x + 1
    y = 0
Next

End Sub

Private Sub Command2_Click()
    Text1.Text = Empty

End Sub

Private Sub Form_Load()
    Text1.Text = Empty
End Sub




